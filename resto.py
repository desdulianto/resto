from flask import Flask, abort, request
from models import db
import models
from hashlib import sha1
from datetime import datetime
from urls import register_urls

#from admin import admin

def create_app(config, defaultConfig='config.DevelopmentConfig'):
    app = Flask(__name__)
    app.config.from_object(defaultConfig)
    app.config.from_pyfile(config, silent=True)

    db.init_app(app)
    db.app = app
    app.db = db 
    db.create_all()

    return app

def init_user():
    user = models.User.query.filter_by(nama='admin').first()
    if user is None:
        user = models.User(nama='admin', password=sha1('admin').hexdigest())
        app.db.session.add(user)
        app.db.session.commit()

app = create_app('config.cfg')
init_user()

register_urls(app)

#app.register_blueprint(admin.bp, url_prefix='/admin/')

if __name__ == '__main__':
    app.run()

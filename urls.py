import views

def register_urls(app):
    add_route = app.add_url_rule

    add_route('/', 'index', views.indexView)
    add_route('/login', 'login', views.loginView, methods=['GET', 'POST'])
    add_route('/list/<table>/', 'list', views.listView)
    add_route('/kategori', 'kategori', views.AddNewView, 
        methods=['GET', 'POST'])
    add_route('/kategori/<nama>', 'detailKategori', views.EditView,
        methods=['GET', 'POST'])
    add_route('/menu', 'menu', views.TambahMenuView,
        methods=['GET', 'POST'])
    add_route('/menu/<nama>', 'detailMenu', views.EditMenuView,
        methods=['GET', 'POST'])

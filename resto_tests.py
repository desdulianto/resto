import unittest
import resto
import models
from hashlib import sha1
from flask import url_for
from flask import Response
from urls import register_urls

class ModelTest(unittest.TestCase):
    def setUp(self):
        self.app = resto.create_app('testconfig.cfg', 'config.TestingConfig')
        with self.app.test_request_context():
            self.app.db.create_all()
        self.db = self.app.db
        register_urls(self.app)

    def tearDown(self):
        pass

class KategoriTest(ModelTest):
    def testGetKategori(self):
        rv = self.app.test_client().get('/kategori', follow_redirects=True)
        assert 'Nama' in rv.data
        assert 'Kategori' in rv.data

    def testTambahKategori(self):
        # tambah kategori baru tanpa kategori parent
        rv = self.app.test_client().post('/kategori', data=dict(
            nama='Makanan', parent_id='-1',
            ), follow_redirects=True)

        kategori = models.Kategori.query.filter_by(nama='Makanan').first()
        assert (kategori is not None)
        assert (kategori.created is not None)
        assert (kategori.modified is None)
        assert (kategori.voided is None)

        # tambah kategori baru dengan kategori parent 1 (makanan)
        rv = self.app.test_client().post('/kategori', data=dict(
            nama='Nasi', parent_id='1',
            ), follow_redirects=True)
        kategori = models.Kategori.query.filter_by(nama='Nasi').first()
        assert kategori is not None
        assert kategori.parent.id == 1

        # tambah kategori baru dengan nama tidak diisi (nama harus diisi)
        rv = self.app.test_client().post('/kategori', data=dict(
            parent_id='1'), follow_redirects=True)
        assert 'Nama harus diisi' in rv.data

        # tambah kategori baru dengan nama '' (nama minimal 1 karakter)
        rv = self.app.test_client().post('/kategori', data=dict(
            nama = '', parent_id='1'), follow_redirects=True)
        assert 'Nama harus diisi' in rv.data

        # tambah kategori baru dengan parent id yang salah (kategori salah)
        rv = self.app.test_client().post('/kategori', data=dict(
            nama='Sate', parent_id='0'), follow_redirects=True)
        assert 'Kategori salah' in rv.data

        # tambah kategori baru dengan nama yang sudah terdaftar (kategori sudah terdaftar)
        rv = self.app.test_client().post('/kategori', data=dict(
            nama='Makanan', parent_id='-1'), follow_redirects=True)
        assert 'sudah terdaftar' in rv.data

        kategori = models.Kategori.query.filter_by(nama='Makanan').all()
        assert len(kategori) == 1

    def testUpdateKategori(self):
        # tambah kategori baru
        rv = self.app.test_client().post('/kategori', data=dict(
            nama='Test', parent_id='-1'), follow_redirects=True)
        kategori = models.Kategori.query.filter_by(nama='Test').first()
        assert kategori is not None

        rv = self.app.test_client().post('/kategori', data=dict(
            nama='Test1', parent_id='-1'), follow_redirects=True)
        kategori = models.Kategori.query.filter_by(nama='Test1').first()
        assert kategori is not None

        parent_id = models.Kategori.query.filter_by(nama='Test').first().id

        rv = self.app.test_client().post('/kategori/test1', data=dict(
            nama='Test1', parent_id=parent_id), follow_redirects=True)
        kategori = models.Kategori.query.filter_by(nama='Test1').first()
        assert kategori.parent.id == parent_id
        assert kategori.modified is not None

        #update kategori yang belum ada
        rv = self.app.test_client().get('/kategori/test2')
        assert rv.status_code == 404


class UserTest(ModelTest):
    def testEmptyDatabase(self):
        with self.app.test_request_context():
            users = models.User.query.all()
            groups = models.Group.query.all()

        assert len(users) == 0, 'Error pada table user'
        assert len(groups) == 0, 'Error pada table group'

    def tambahUser(self):
        with self.app.test_request_context():
            user = models.User(nama='admin', 
                password=sha1('admin').hexdigest())
            self.db.session.add(user)
            self.db.session.commit()

    def deleteUsers(self):
        with self.app.test_request_context():
            users = models.User.query.all()
            for i in users:
                del users
            self.db.session.commit()

    def testTambahUser(self):
        self.tambahUser()
        with self.app.test_request_context():
            user = models.User.query.filter_by(nama='admin').first()
            assert user.nama == 'admin'
            assert user.created is not None
            assert user.modified is None
            assert user.voided is None
            self.deleteUsers()

    def testUpdateUser(self):
        self.tambahUser()
        with self.app.test_request_context():
            user = models.User.query.filter_by(nama='admin').first()
            user.nama = 'Admin'
            self.db.session.commit()

            user = models.User.query.filter_by(nama='Admin').first()
            assert user.nama == 'Admin'
            assert user.created is not None
            assert user.modified is not None
            assert user.created != user.modified


class MenuTest(ModelTest):
    def setUp(self):
        super(MenuTest, self).setUp()
        self.menu_url = None
        with self.app.test_request_context():
            self.menu_url = url_for('menu')

    def testUrl(self):
        assert self.menu_url is not None

    def testGet(self):
        rv = self.app.test_client().get(self.menu_url)
        assert 'Nama' in rv.data
        assert 'Kategori' in rv.data
        assert 'Harga' in rv.data

    def testAddNewMenu(self):
        with self.app.test_client() as c:
            k = models.Kategori(nama='Makanan', parent=None)
            self.app.db.session.add(k)
            self.app.db.session.commit()
            k_id = k.id

            k1 = models.Kategori(nama='Minuman', parent=None)
            self.app.db.session.add(k1)
            self.app.db.session.commit()
            k1_id = k1.id

            # tambah menu baru
            rv = c.post(self.menu_url, data=dict(
                nama='Nasi Goreng', kategori_id=k_id, harga=10000),
                follow_redirects=True)
            m = models.Menu.query.filter_by(nama='Nasi Goreng').first()
            assert m is not None

            # tambah menu baru dengan nama yang salah
            rv = c.post(self.menu_url, data=dict(
                nama='', kategori_id=k_id, harga=10000),
                follow_redirects =True)
            assert 'Nama salah' in rv.data
            m = models.Menu.query.filter_by(nama='').all()
            assert len(m) == 0

            # tambah menu baru dengan kategori yang salah
            rv = c.post(self.menu_url, data=dict(
                nama='Nasi Goreng1', kategori_id=-1, harga=10000),
                follow_redirects=True)
            assert 'Kategori salah' in rv.data
            m = models.Menu.query.filter_by(nama='Nasi Goreng1').first()
            assert m is None

            # tambah menu baru dengan harga yang salah
            rv = c.post(self.menu_url, data=dict(
                nama='Nasi Goreng2', kategori_id=k_id, harga=0),
                follow_redirects=True)
            assert 'Harga salah' in rv.data
            m = models.Menu.query.filter_by(nama='Nasi Goreng2').first()
            assert m is None

            # tambah menu baru dengan nama yang sudah terdaftar pada kategori
            # yang sama
            rv = c.post(self.menu_url, data=dict(
                nama='Nasi Goreng', kategori_id=k_id, harga=10000),
                follow_redirects=True)
            assert 'Menu Nasi Goreng sudah terdaftar' in rv.data
            m = models.Menu.query.filter_by(nama='Nasi Goreng').all()
            assert len(m) == 1


            # FIXME k1 detached from session
            #with self.app.test_request_context():
            #    k1 = models.Kategori.query.filter_by(nama='Minuman').first()
            #    k1_id = k1.id
            #    # tambah menu baru dengan nama yang sudah terdaftar pada kategori
            #    # yang berbeda
            #    rv = c.post(self.menu_url, data=dict(
            #        nama='Nasi Goreng', kategori_id=k1_id, harga=10000),
            #        follow_redirects=True)
            #    m = (models.Menu.query.filter_by(nama='Nasi Goreng').
            #        filter((models.Menu.kategori.contains(k1))).first())
            #    assert m is not None

    def testUpdate(self):
        k = models.Kategori(nama='Makanan', parent=None)
        models.db.session.add(k)
        models.db.session.commit()
        k_id = k.id

        m = models.Menu(nama='Nasi Goreng', harga=10000, kategori=[k])
        models.db.session.add(m)
        models.db.session.commit()

        url = '%s/%s' % (self.menu_url, m.nama.replace(' ', '-').lower())

        # ubah harga
        rv = self.app.test_client().post(url, data=dict(
            nama=m.nama, kategori_id=m.kategori[0].id, harga=2000),
            follow_redirects=True)
        m = models.Menu.query.filter_by(nama='Nasi Goreng').first()
        assert m.harga == 2000

        # ubah harga salah
        rv = self.app.test_client().post(url, data=dict(
            nama=m.nama, kategori_id=m.kategori[0].id, harga=0),
            follow_redirects=True)
        assert 'Harga salah' in rv.data

        # ubah kategori salah
        rv = self.app.test_client().post(url, data=dict(
            nama=m.nama, kategori_id=-1, harga=10000),
            follow_redirects=True)
        assert 'Kategori salah' in rv.data

        # ubah menu yang tidak ada
        rv = self.app.test_client().get(self.menu_url + '/mie-goreng')
        assert rv.status_code == 404

if __name__ == '__main__':
    unittest.main()

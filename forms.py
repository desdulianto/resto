from flask.ext.wtf import (Form, Required, ValidationError, Length,
    NumberRange)
from flask.ext.wtf import (TextField, PasswordField, SelectField,
    DecimalField)
import models
from wtforms.ext.appengine.db import model_form

class LoginForm(Form):
    username = TextField('User', validators=[Required('Nama harus diisi'),
        Length(min=1, message='Nama minimal 1 karakter')])
    password = PasswordField('Password', validators=[Required()])

class KategoriForm(Form):
    nama = TextField('Nama', validators=[Required('Nama harus diisi'),
        Length(min=1, message='Nama minimal 1 karakter')])
    parent_id= SelectField('Kategori', coerce=int)
    
    def __init__(self, formdata=None, obj=None, prefix='', **kwargs):
        super(KategoriForm, self).__init__(formdata, obj, prefix, **kwargs)
        self.parent_id.choices = [(-1, '-- Select Kategori --')]

        if obj is None:
            self.parent_id.choices.extend( [(x.id, x.nama) 
                for x in models.Kategori.query.order_by('nama') ])
        else:
            self.parent_id.choices.extend( [(x.id, x.nama) 
                for x in models.Kategori.query.filter(
                    (models.Kategori.id != obj.id)).order_by('nama') ])
 
    def validate_parent_id(form, field):
        if field.data not in [x[0] for x in field.choices]:
            raise ValidationError('Kategori salah')

        parent = models.Kategori.query.filter_by(id=field.data).first()
        form.parent = parent

class MenuForm(Form):
    nama = TextField('Nama', validators=[Required('Nama salah'),
        Length(min=1, message='Nama salah')])
    harga = DecimalField('Harga', validators=[
        NumberRange(min=1, message='Harga salah')])
    kategori_id = SelectField('Kategori', coerce=int)

    def __init__(self, formdata=None, obj=None, prefix='', **kwargs):
        super(MenuForm, self).__init__(formdata, obj, prefix, **kwargs)
        self.kategori_id.choices = [(-1, '-- Select Kategori --')]

        self.kategori_id.choices.extend( [(x.id, x.nama)
            for x in models.Kategori.query.order_by('nama')] )

        if obj is not None and formdata.get('kategori_id') is None:
            self.kategori_id.data = obj.kategori[0].id

    def validate_kategori_id(form, field):
        if field.data not in [x[0] for x in field.choices if x[0] != -1]:
            raise ValidationError('Kategori salah')

        kategori = models.Kategori.query.filter_by(id=field.data).first()
        form.kategori = kategori

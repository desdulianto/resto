from datetime import datetime
from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Document(object):
    created = db.Column(db.DateTime(timezone=True), default=datetime.utcnow)
    modified = db.Column(db.DateTime(timezone=True), onupdate=datetime.utcnow)
    voided = db.Column(db.DateTime(timezone=True))

    def __init__(self):
        if self.created is None:
            self.created = datetime.utcnow()

class User(Document, db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(80), unique=True, index=True)
    password = db.Column(db.String(40), nullable=False)

    def __init__(self, nama, password):
        Document.__init__(self)
        self.nama = nama
        self.password = password

    def __repr__(self):
        return '<User %s>' % self.nama


class Group(Document, db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(80), unique=True, index=True)

    def __init__(self, nama):
        Document.__init__(self)
        self.nama = nama

    def __repr__(self):
        return '<Group %s>' % self.nama

kategori_menu = db.Table('kategorimenu',
    db.Column('menu_id', db.Integer, db.ForeignKey('menu.id')),
    db.Column('kategori_id', db.Integer, db.ForeignKey('kategori.id')))

class Kategori(Document, db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('kategori.id'))
    nama = db.Column(db.String(80), unique=True, index=True)
    children = db.relationship('Kategori', backref=db.backref('parent', 
        remote_side=[id]))

    def __init__(self, nama, parent):
        self.nama = nama
        self.parent = parent

    def __repr__(self):
        return '%s' % self.nama

    def __str__(self):
        return self.__repr__()

class Menu(Document, db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(80), unique=True, index=True)
    harga = db.Column(db.Numeric)
    kategori = db.relationship(Kategori, secondary='kategorimenu', 
        backref='menu')

    def __init__(self, nama, harga, kategori):
        self.nama = nama
        self.harga = harga
        self.kategori = kategori

    def __repr__(self):
        return '%s' % self.nama

    def __str__(self):
        return repr(self)

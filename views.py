from flask import request
from flask import render_template
from flask import flash
from flask import abort
from flask import redirect, url_for
import models
from hashlib import sha1

from sqlalchemy.exc import IntegrityError

from forms import LoginForm, KategoriForm, MenuForm
from werkzeug.datastructures import ImmutableMultiDict

def indexView():
    return render_template('base.html')

def loginView():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        user = models.User.query.filter_by(nama=username).first()
        if user is None:
            return 'User/Password salah', 403
        if user.password != sha1(password).hexdigest():
            return 'User/Password salah', 403
        return 'Authentication success', 200
    return render_template('login.html', form=LoginForm())

def listView(table):
    model = getattr(models, table.title(), None)
    if model is None:
        abort(404)
    else:
        data = model.query.order_by('id').all()
        return render_template('list.html', data=data)

def AddNewView():
    form = KategoriForm(request.form)

    if form.validate_on_submit():
        try:
            kategori = models.Kategori(nama=form.nama.data.title(), 
                parent=form.parent)
            models.db.session.add(kategori)
            models.db.session.commit()
            flash('Kategori %s berhasil disimpan' % form.nama.data)
        except IntegrityError:
            models.db.session.rollback()
            flash('Kategori %s sudah terdaftar' % form.nama.data)
        except Exception, e:
            print e.__class__, e
            return abort(500)
        return redirect(url_for('kategori'))
    return render_template('genericform.html', form=form)

def EditView(nama=None):
    kategori = models.Kategori.query.filter_by(
        nama=nama.replace('-', ' ').title()).first()
    if kategori is None:
        return abort(404)

    form = KategoriForm(request.form, kategori)
    # FIXME disable input for nama

    if form.validate_on_submit():
        try:
            kategori.parent = form.parent
            models.db.session.commit()
            flash('Kategori %s berhasil diupdate' % form.nama.data)
        except IntegrityError:
            models.db.session.rollback()
            flash('Kategori %s kesalahan integritas database' % 
                form.nama.data)
        except:
            print e.__class__, e
            return abort(500)
        return redirect(url_for('kategori') + '/%s' % nama)
    return render_template('genericform.html', form=form)

def TambahMenuView():
    form = MenuForm(request.form)

    if form.validate_on_submit():
        try:
            menu = models.Menu(nama=form.nama.data.title(), 
                harga=form.harga.data, kategori=[form.kategori])
            models.db.session.add(menu)
            models.db.session.commit()
            flash('Menu %s berhasil disimpan' % form.nama.data)
        except IntegrityError:
            models.db.session.rollback()
            flash('Menu %s sudah terdaftar' % form.nama.data)
        except Exception, e:
            print e.__class__, e
            return abort(500)
        return redirect(url_for('menu'))
    return render_template('genericform.html', form=form)

def EditMenuView(nama=None):
    menu = models.Menu.query.filter_by(
        nama=nama.replace('-', ' ').title()).first()
    if menu is None:
        return abort(404)

    form = MenuForm(request.form, menu)
    # FIXME disable input for nama

    if form.validate_on_submit():
        try:
            menu.harga = form.harga.data
            menu.kategori = [form.kategori]
            models.db.session.commit()
            flash('Kategori %s berhasil diupdate' % form.nama.data)
        except IntegrityError:
            models.db.session.rollback()
            flash('Kategori %s kesalahan integritas database' % 
                form.nama.data)
        except:
            print e.__class__, e
            return abort(500)
        return redirect(url_for('menu') + '/%s' % nama)
    return render_template('genericform.html', form=form)

